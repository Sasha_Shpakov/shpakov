package lab6.task1;

import java.io.File;
import java.nio.file.Files;

public class MyThread implements Runnable {
    public String input;
    public String dirPath = ".";
    public File dir = new File(dirPath);
    public String copydir;

    public MyThread(String input) {
        this.input = input;
    }

    public void method(File dir) throws Exception{

        for (File i : dir.listFiles()) {
            if (!i.isDirectory()) {
                if (i.getName().contains(input)) {
                    char[] arr = Thread.currentThread().getName().toCharArray();
                    int i1 = Character.getNumericValue(arr[arr.length - 1]);
                    copydir = i.getParent() + (i1 + 1);
                    File f1 = new File(copydir);
                    f1.mkdir();
                    File f2 = new File(copydir + "/" + i.getName());
                    Files.copy(i.toPath(), f2.toPath());
                }
            }
            else {
                method (i);
            }
        }
    }

    @Override
    public void run() {
        try {
            Thread.sleep(10000);
            method(dir);
            if (copydir == null) {
                System.out.println(Thread.currentThread().getName() + " There is no file with this word.");
            }
            else {
                System.out.println(Thread.currentThread().getName() + " Copying is finished.");
            }
        }catch (Exception e){
            System.out.println(Thread.currentThread().getName() + " Exception is occured.");
        }
    }
}

