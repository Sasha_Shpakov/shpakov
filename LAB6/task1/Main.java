package lab6.task1;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Please enter a word.");
            String userInput = reader.readLine();
            if (userInput.equals("stop")) {
                break;
            }
            else {
                new Thread(new MyThread(userInput)).start();
            }
        }
    }
}
