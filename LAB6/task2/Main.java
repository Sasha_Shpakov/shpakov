package lab6.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Main {
    static Queue<Integer> queue = new ArrayBlockingQueue<>(10);
    static List<Integer> arr = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException{

        Thread even = new Thread (new Even());
        even.start();

        Thread odd = new Thread (new Odd());
        odd.start();

        Thread sum = new Thread (new Sum());
        sum.start();

        for (int i = 1; i <= 10; i++) {
            Thread.sleep(1000);
            synchronized (queue) {
                arr.add(i);
                queue.offer(i);
                queue.notifyAll();
            }
        }

        System.exit(0);
    }
}
