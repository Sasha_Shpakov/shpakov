package lab6.task2;


public class Odd extends Main implements Runnable {
    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                }
                if(queue.peek() != null) {
                    int i = queue.peek();
                    if (i%2 != 0) {
                        System.out.println(Thread.currentThread().getName() + " - Odd number = " + queue.poll());
                        synchronized (arr){
                            arr.notifyAll();
                        }
                    }
                }
            }
        }
    }
}
