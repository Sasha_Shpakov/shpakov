package lab6.task2;


public class Sum extends Main implements Runnable {
    @Override
    public void run() {
        while (true) {
            synchronized (arr) {
                try {
                    arr.wait();
                } catch (InterruptedException e) {
                }
                int sum = 0;
                for(int i : arr){
                    sum += i;
                }
                System.out.println(Thread.currentThread().getName() + " - Current sum = " + sum);
            }
        }
    }
}