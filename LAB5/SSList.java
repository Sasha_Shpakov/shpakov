package lab5.task1;


import java.util.*;

public class SSList<E> implements List {
    public Object ssList[];
    public int size = 0;

    public SSList() {
        ssList = new Object[size];
    }

    public SSList(int capacity) {
        ssList = new Object[capacity];
        size = ssList.length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (ssList.length == 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean contains(Object o) {
        boolean c = false;
        for (int i = 0; i < size; i++) {
            if (o.equals(ssList[i])) {
                c = true;
                break;
            }
        }
        return c;
    }

    public Iterator iterator() {
        return new NewIterator();
    }

    private class NewIterator implements Iterator<E> {
        int nextel;
        int lastel = -1;

        @Override
        public boolean hasNext() {
            return nextel != size;
        }

        @Override
        public E next() {
            Object next = get(nextel);
            lastel = nextel++;
            return (E)next;
        }

        @Override
        public void remove() {
            SSList.this.remove(lastel);
            nextel = lastel;
            lastel = -1;
        }
    }


    @Override
    public Object[] toArray() {
        return Arrays.copyOf(ssList, size);
    }

    @Override
    public boolean add(Object o) {
        ssList = Arrays.copyOf(ssList, size + 1);
        size++;
        ssList[size - 1] = o;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(ssList[i])) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        Object[] arr = c.toArray();
        Object[] ss = ssList;
        Object[] test = new Object[size() + arr.length];
        System.arraycopy(ss, 0, test, 0, ss.length);
        System.arraycopy(arr, 0, test, ss.length, arr.length);
        ssList = test;
        size += arr.length;
        return true;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        Object[] arr = c.toArray();
        Object[] ss = ssList;
        Object[] ss1 = new Object[index];
        System.arraycopy(ss, 0, ss1, 0, index);
        Object[] ss2 = new Object[ss.length - index];
        System.arraycopy(ss, index, ss2, 0, ss.length - index);
        Object[] test = new Object[size() + arr.length];
        System.arraycopy(ss1, 0, test, ss.length, ss1.length);
        System.arraycopy(arr, 0, test, ss1.length, arr.length);
        System.arraycopy(ss2, 0, test, ss1.length + arr.length, ss2.length);
        ssList = test;
        size += arr.length;
        return true;
    }

    @Override
    public void clear() {
        ssList = new Object[size];
        size = 0;
    }

    @Override
    public Object get(int index) {
        return ssList[index];
    }

    @Override
    public Object set(int index, Object element) {
        Object tmp = ssList[index];
        ssList[index] = element;
        return tmp;
    }

    @Override
    public void add(int index, Object element) {
        ssList = Arrays.copyOf(ssList, size + 1);
        System.arraycopy(ssList,index,ssList,index + 1,size - index);
        ssList[index] = element;
        size++;
    }

    @Override
    public Object remove(int index) {
        Object tmp = ssList[index];
        System.arraycopy(ssList,index+1,ssList,index,size - index - 1);
        ssList = Arrays.copyOf(ssList, size - 1);
        size--;
        return tmp;
    }


    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(ssList[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size - 1; i > 0; i--) {
            if (o.equals(ssList[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ListIterator listIterator() {
        return new NewListIterator(0);
    }

    @Override
    public ListIterator listIterator(int index) {
        return new NewListIterator(index);
    }

    private class NewListIterator extends NewIterator implements ListIterator<E> {
        NewListIterator (int index)
        {
            super();
            nextel = index;
        }

        @Override
        public boolean hasPrevious() {
            return nextel != 0;
        }

        @Override
        public E previous() {
            Object previous = get(nextel);
            lastel = nextel++;
            return (E)previous;
        }

        @Override
        public int nextIndex() {
            return nextel;
        }

        @Override
        public int previousIndex() {
            return nextel - 1;
        }

        @Override
        public void set(E e) {
            SSList.this.set(lastel,e);
        }

        @Override
        public void add(E e) {
            SSList.this.add(nextel,e);
            nextel++;
            lastel = -1;
        }
    }


    @Override
    public List subList(int fromIndex, int toIndex) {
        Object[] tmp = Arrays.copyOfRange(ssList,fromIndex,toIndex);
        List subList = Arrays.asList(tmp);
        return subList;
    }

    @Override
    public boolean retainAll(Collection c) {
        Object[] arr = c.toArray();
        boolean c1 = false;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (ssList[i].equals(arr[j])) {
                    c1 = true;
                    break;
                }
            }
            if (c1 != true) {
                remove(i);
                i--;
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection c) {
        Object[] arr = c.toArray();
        boolean c2 = false;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (ssList[i].equals(arr[j])) {
                    remove(i);
                    c2 = true;
                    break;
                }
            }
            if (c2) {
                i--;
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        Object[] arr = c.toArray();
        boolean c3 = true;
        for (int j = 0; j < arr.length; j++)
            if (indexOf(arr[j]) == -1) {
                c3 = false;
                break;
            }
        return c3;
    }

    @Override
    public Object[] toArray(Object[] a) {
        if (a.length < size)
            return (Arrays.copyOf(ssList, size));
        System.arraycopy(ssList, 0, a, 0, size);
        return a;
    }
}
