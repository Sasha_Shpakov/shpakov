package lab5.task1;

import java.util.List;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        List<LocalDate> list = new SSList();
        for (int i = 1; i < 31; i++) {
            list.add(LocalDate.of(1988, 10, i));
        }
        for (int i = 0; i < list.size(); i++) {
            try {
                check(list.get(i));
            } catch (HolidayException e)
            {
                System.out.println(list.get(i).toString() + " - " + list.get(i).getDayOfWeek() + " - Holiday");
            } catch (RegulardayException e)
            {
                System.out.println(list.get(i).toString() + " - " + list.get(i).getDayOfWeek() + " - Regularday");
            }

        }
    }

    public static void check(LocalDate ld) throws RegulardayException, HolidayException {
        if (ld.getDayOfWeek().toString().equals("SATURDAY") || ld.getDayOfWeek().toString().equals("SUNDAY")) {
            throw new HolidayException();
        } else {
            throw new RegulardayException();
        }
    }

}