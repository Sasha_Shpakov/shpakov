package lab4.task1;

public abstract class Car {
    public String name; // название машины
    public double v0 = 0; // начальная скорость
    public double vmax; // максимальная скорость
    public double vmaxt; // максимальная скорость в м/с
    public double acc; // ускорение
    public double man; // маневренность
    public double v; // конечная скорость
    public double ds = 2000; // длина прямого отрезка
    public double t = 0; // время
    public double s = 0; // расстояние пройденное до набора максимальной скорости

    public Car(String name, double vmax, double acc, double man)
    {
        this.name = name;
        this.vmax = vmax;
        this.acc = acc;
        this.man = man;
        this.vmaxt = vmax * 1000 / 3600;
    }

    public void race() {
        for (int i = 1; i <= 20; i++) {
            direct();
            turn();
        }
    }

    public void direct() {
        v = v0 + Math.sqrt((v0 * v0) + (2 * acc * ds));
        if (v < vmaxt) {
            t = t + (v - v0) / acc;
        }
        else {
            s = (vmaxt * vmaxt - v0 * v0) / (2 * acc);
            t = t + (vmaxt - v0) / acc + (ds - s) / vmaxt;
            v = vmaxt;
        }
    }

    public void turn() {
        v0 = v * man;
    }
}