package lab4.task1;

public class Main {
    public static void main(String[] args) {
        Car c1 = new Car1("c1", 180, 0.6, 0.4);
        Car c2 = new Car2("c2", 220, 0.4, 0.6);
        Car c3 = new Car3("c3", 320, 0.7, 0.2);
        Car[] arr = {c1, c2, c3};
        for (Car x : arr) {
            x.race();
        }
        for(int i = 0; i < arr.length-1; i++){
            for(int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j].t > arr[j + 1].t) {
                    Car tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        for (Car i : arr)
        {
            System.out.println("Автомобиль " + i.name + " прошел трассу за " + Math.round(i.t/60) + " минут " + Math.round(i.t%60) + " секунд");
        }
    }
}