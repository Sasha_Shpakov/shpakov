package lab2.task2;


public class Lab2Task2 {
    public static void main(String[] args) {
        byte b = 5;
        short s = 256;
        int i =  1000;
        long l = 21500;
        float f = 44.44f;
        double d = 999.999;
        boolean boo = true;
        char c = 'a';

        String str_b = Byte.toString(b);
        String str_s = Short.toString(s);
        String str_i = Integer.toString(i);
        String str_l = Long.toString(l);
        String str_f = Float.toString(f);
        String str_d = Double.toString(d);
        String str_boo = Boolean.toString(boo);
        String str_c = Character.toString(c);

        byte b_a = Byte.parseByte(str_b);
        short s_a = Short.parseShort(str_s);
        int i_a = Integer.parseInt(str_i);
        long l_a = Long.parseLong(str_l);
        float f_a = Float.parseFloat(str_f);
        double d_a = Double.parseDouble(str_d);
        boolean boo_a = Boolean.parseBoolean(str_boo);
        char c_a = str_c.charAt(0);

        System.out.println("ПРЕОБРАЗОВАНИЕ ПРИМИТИВНЫХ ТИПОВ ДАННЫХ В СТРОКИ И ОБРАТНО");
        System.out.println("");
        System.out.println("Переменная типа byte b = " + b + ": b + 2 = " + b + " + " + 2 + " = " + (b + 2));
        System.out.println("После преобразования в строку: b + 2 = " + str_b + " + " + 2 + " = " + (str_b + 2));
        System.out.println("После преобразования обратно: b + 2 = " + b_a + " + " + 2 + " = " + (b_a + 2));
        System.out.println("");
        System.out.println("Переменная типа short s = " + s + ": s + 2 = " + s + " + " + 2 + " = " + (s + 2));
        System.out.println("После преобразования в строку: s + 2 = " + str_s + " + " + 2 + " = " + (str_s + 2));
        System.out.println("После преобразования обратно: s + 2 = " + s_a + " + " + 2 + " = " + (s_a + 2));
        System.out.println("");
        System.out.println("Переменная типа int i = " + i + ": i + 2 = " + i + " + " + 2 + " = " + (i + 2));
        System.out.println("После преобразования в строку: i + 2 = " + str_i + " + " + 2 + " = " + (str_i + 2));
        System.out.println("После преобразования обратно: i + 2 = " + i_a + " + " + 2 + " = " + (i_a + 2));
        System.out.println("");
        System.out.println("Переменная типа long l = " + l + ": l + 2 = " + l + " + " + 2 + " = " + (l + 2));
        System.out.println("После преобразования в строку: l + 2 = " + str_l + " + " + 2 + " = " + (str_l + 2));
        System.out.println("После преобразования обратно: l + 2 = " + l_a + " + " + 2 + " = " + (l_a + 2));
        System.out.println("");
        System.out.println("Переменная типа float f = " + f + ": f + 2 = " + f + " + " + 2 + " = " + (f + 2));
        System.out.println("После преобразования в строку: f + 2 = " + str_f + " + " + 2 + " = " + (str_f + 2));
        System.out.println("После преобразования обратно: f + 2 = " + f_a + " + " + 2 + " = " + (f_a + 2));
        System.out.println("");
        System.out.println("Переменная типа double d = " + d + ": d + 2 = " + d + " + " + 2 + " = " + (d + 2));
        System.out.println("После преобразования в строку: d + 2 = " + d + " + " + 2 + " = " + (str_d + 2));
        System.out.println("После преобразования обратно: d + 2 = " + d_a + " + " + 2 + " = " + (d_a + 2));
        System.out.println("");
        System.out.println("Переменная типа boolean boo = " + boo);
        System.out.println("После преобразования в строку:  boo + 2 = " + str_boo + " + " + 2 + " = " + (str_boo + 2));
        System.out.println("После преобразования обратно boo = " + boo_a);
        System.out.println("");
        System.out.println("Переменная типа char c = " + c + ": c + 2 = " + c + " + " + 2 + " = " + (c + 2));
        System.out.println("После преобразования в строку: c + 2 = " + str_c + " + " + 2 + " = " + (str_c + 2));
        System.out.println("После преобразования обратно: c + 2 = " + c_a + " + " + 2 + " = " + (c_a + 2));
    }
}
