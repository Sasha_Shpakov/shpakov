package lab2.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Lab2Task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите целое число");
        int a = Integer.parseInt(reader.readLine());
        System.out.println("Введенное число типа int = " + a);
        float a_f = (float)a;
        System.out.println("После преобразование в тип float = " + a_f);
        System.out.println("");

        System.out.println("Введите нецелое число с точкой");
        double d = Double.parseDouble(reader.readLine());
        System.out.println("Введенное число типа double = " + d);
        long d_l = (long)d;
        System.out.println("После преобразование в тип long = " + d_l);
    }
}
