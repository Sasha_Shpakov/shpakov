package lab2.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Lab2Task1 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите целое число a");
        int a = Integer.parseInt(reader.readLine());
        System.out.println("Введите целое число b");
        int b = Integer.parseInt(reader.readLine());

        System.out.println("");
        System.out.println("ВВЕДЕННЫЕ ЧИСЛА");
        System.out.println("a = " + a);
        System.out.println("b = " + b);

        int sum = (a + b);
        int sub = (a - b);
        int mult = (a * b);
        double div = (1.0*a/ b);
        int mod = (a % b);

        System.out.println("");
        System.out.println("АРИФМЕТИЧЕСКИЕ ОПЕРАЦИИ");
        System.out.println("a + b = " + sum);
        System.out.println("a - b = " + sub);
        System.out.println("a * b = " + mult);
        System.out.println("a / b = " + div);
        System.out.println("a % b = " + mod);

        System.out.println("");
        System.out.println("СРАВНЕНИЕ ЧИСЕЛ");
        if (a < b) {
            System.out.println("a меньше b");
        }
        else if (a > b) {
            System.out.println("a больше b");
        }
        else {
            System.out.println("a равно b");
        }

        System.out.println("");
        System.out.println("ПРОВЕРКА НА ЧЁТНОСТЬ");
        if (a%2 == 0 && b%2 == 0) {
            System.out.println("a и b - четные числа");
        }
        else if (a%2 == 0 && b%2 != 0) {
            System.out.println("a - четное число, b - нечетное число");
        }
        else if (a%2 != 0 && b%2 == 0) {
            System.out.println("a - нечетное число, b - четное число");
        }
        else {
            System.out.println("a и b - нечетные числа");
        }

    }
}
