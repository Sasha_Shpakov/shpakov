package lab2.task4;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab2Task4 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число a");
        double a = Double.parseDouble(reader.readLine());
        System.out.println("Введите знак математической операции:");
        System.out.println("+ сложение");
        System.out.println("- вычитание");
        System.out.println("* умножение");
        System.out.println("/ деление");
        System.out.println("% остаток от деления");
        String oper = reader.readLine();
        System.out.println("Введите число b");
        double b = Double.parseDouble(reader.readLine());

        switch (oper) {
            case "+":
                System.out.println("РЕЗУЛЬТАТ: a + b = " + (a + b));
                break;
            case "-":
                System.out.println("РЕЗУЛЬТАТ: a - b = " + (a - b));
                break;
            case "*":
                System.out.println("РЕЗУЛЬТАТ: a * b = " + (a * b));
                break;
            case "/":
                System.out.println("РЕЗУЛЬТАТ: a / b = " + (a / b));
                break;
            case "%":
                System.out.println("РЕЗУЛЬТАТ: a % b = " + (a % b));
                break;
            default:
                System.out.println("РЕЗУЛЬТАТ: Некорректная операция");
        }
    }
}
