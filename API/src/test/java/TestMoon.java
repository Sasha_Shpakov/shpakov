import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

public class TestMoon {
    public static HashMap<String, String> reqmap = new HashMap<String, String>();

    @BeforeClass
    public static void setUp() throws IOException {
        reqmap = Main.readFile();
    }

    @Test
    public void TestQuarter() throws Exception{
        for (HashMap.Entry<String, String> entry : reqmap.entrySet()) {
            Assert.assertTrue(entry.getKey() + " doesn't have expected word",Main.isQuarterPresent(entry.getKey(), entry.getValue()));
        }
    }
}
