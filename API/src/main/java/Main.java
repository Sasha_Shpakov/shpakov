import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class Main {
    public static HashMap<String, String> readFile() throws IOException {
        String fileName = "./test.txt";
        HashMap<String, String> requests = new HashMap<String, String>();
        File file = new File(fileName);
        BufferedReader in = new BufferedReader(new java.io.FileReader(file));
        String s;
        while ((s = in.readLine()) != null) {
            String[] line = s.split(" {3}");
            requests.put(line[0],line[1]);
        }
        in.close();
        return requests;
    }

    public static String getResponse(String request) throws IOException {
        URL url = new URL(request);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() != 203) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + conn.getResponseCode());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String output;
        String temp = "";
        while ((output = br.readLine()) != null) {
            temp += output;
        }
        conn.disconnect();
        return temp;
    }

    public static boolean isQuarterPresent(String request, String expected) throws IOException {
        boolean isQuarter;
        String tmp = getResponse(request);
        if (tmp.contains(expected)) {
            isQuarter = true;
        } else {
            isQuarter = false;
        }
        return isQuarter;
    }
}
