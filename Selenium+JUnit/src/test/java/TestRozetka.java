import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class TestRozetka {
    static WebDriver driver;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @Before
    public void setUp() throws Exception {
        driver.get("http://rozetka.com.ua");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void test1() throws Exception {
        WebElement img_RozetkaMain_Logo = driver.findElement(By.xpath(".//div[@class='logo']"));
        assertTrue("Unable to find logo", img_RozetkaMain_Logo.isDisplayed());
    }

    @Test
    public void test2() throws Exception {
        WebElement lnk_RozetkaMain_Apple = driver.findElement(By.xpath(".//ul[@id='m-main']/li[a[contains(text(),'Apple')]]"));
        assertTrue("Unable to find Apple", lnk_RozetkaMain_Apple.isDisplayed());
    }

    @Test
    public void test3() throws Exception {
        WebElement lnk_RozetkaMain_MP3 = driver.findElement(By.xpath(".//ul[@id='m-main']/li[a[contains(text(),'MP3')]]"));
        assertTrue("Unable to find MP3", lnk_RozetkaMain_MP3.isDisplayed());
    }

    @Test
    public void test4() throws Exception {
        WebElement lnk_RozetkaMain_City = driver.findElement(By.xpath(".//span[@class='xhr bold']"));
        lnk_RozetkaMain_City.click();
        WebElement lnk_CityPopup_CityKharkiv = driver.findElement(By.xpath(".//div[@class='clearfix']//*[contains(text(),'Харьков')]"));
        WebElement lnk_CityPopup_CityOdessa = driver.findElement(By.xpath(".//div[@class='clearfix']//*[contains(text(),'Одесса')]"));
        WebElement lnk_CityPopup_CityKiev = driver.findElement(By.xpath(".//div[@class='clearfix']//*[contains(text(),'Киев')]"));
        assertTrue("Unable to find Kharkiv", lnk_CityPopup_CityKharkiv.isDisplayed());
        assertTrue("Unable to find Odessa", lnk_CityPopup_CityOdessa.isDisplayed());
        assertTrue("Unable to find Kiev", lnk_CityPopup_CityKiev.isDisplayed());
    }

    @Test
    public void test5() throws Exception {
        WebElement lnk_RozetkaMain_Bin = driver.findElement(By.xpath(".//div[contains(@id,'cart_block')]"));
        lnk_RozetkaMain_Bin.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement elem_BinPopup_Empty = driver.findElement(By.xpath(".//div[@id='drop-block']"));
        assertTrue("Bin isn't empty", elem_BinPopup_Empty.isDisplayed());
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test completed");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        driver.quit();
    }
}