import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class TestStackoverflow {
    static WebDriver driver;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @Before
    public void setUp() throws Exception {
        driver.get("http://stackoverflow.com");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void test1() throws Exception {
        WebElement lnk_StackMain_Number = driver.findElement(By.xpath(".//span[@class='bounty-indicator-tab']"));
        assertTrue("Number is less than or equal 300", Integer.parseInt(lnk_StackMain_Number.getText()) > 300);
    }

    @Test
    public void test2() throws Exception {
        WebElement lnk_StackMain_SignUp = driver.findElement(By.xpath(".//span[@class='topbar-menu-links']/a[text()='sign up']"));
        lnk_StackMain_SignUp.click();
        WebElement btn_SignUp_Google = driver.findElement(By.xpath(".//div[@id='openid-buttons']//span[text()='Google']"));
        WebElement btn_SignUp_Facebook = driver.findElement(By.xpath(".//div[@id='openid-buttons']//span[text()='Facebook']"));
        assertTrue("Unable to find Google button", btn_SignUp_Google.isDisplayed());
        assertTrue("Unable to find Facebook button", btn_SignUp_Facebook.isDisplayed());
    }

    @Test
    public void test3() throws Exception {
        WebElement lnk_StackMain_Question = driver.findElement(By.xpath(".//div[@id='question-mini-list']/div[1]//h3"));
        lnk_StackMain_Question.click();
        WebElement lnk_Question_Date = driver.findElement(By.xpath(".//*[@id='qinfo']//b"));
        assertTrue("Question wasn't asked today", lnk_Question_Date.getText().equals("today"));
    }

    @Test
    public void test4() throws Exception {
        WebElement elem_StackMain_Job = driver.findElement(By.xpath(".//div[@class='title']"));
        String title = elem_StackMain_Job.getText();
        int i = title.lastIndexOf('$');
        if (i >= 0) {
            int salary = Integer.parseInt(title.substring(i + 1, title.length() - 1));
            assertTrue("There is no job offer with salary more than $100K", salary > 100);
        }
        else {
            System.out.println("There is no job offer");
        }
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test completed");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        driver.quit();
    }
}