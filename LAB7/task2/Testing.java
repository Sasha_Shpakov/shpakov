package lab7.task2;

import lab7.task1.Numbers;
import lab7.task1.WorkWithNumbers;
import org.junit.Assert;
import org.junit.Test;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Testing {

    @Test
    public void serialize() {
        Numbers n = new Numbers();
        File f = new File("numbers.txt");
        try {
            WorkWithNumbers.serialize(n);
        } catch (IOException e) {
            System.out.println(e);
        }

        Assert.assertTrue(f.exists());
    }

    @Test
    public void deserialize() {
        Numbers n = new Numbers();

        try {
            WorkWithNumbers.serialize(n);
        } catch (IOException e) {
            System.out.println(e);
        }

        try {
            n = WorkWithNumbers.deserialize("numbers.txt");
        } catch (IOException e) {
            System.out.println(e);
        }

        Assert.assertEquals(4, n.num1);
        Assert.assertEquals(44, n.num2);
        Assert.assertEquals(444, n.num3);

    }

    @Test
    public void work() {
        Numbers n = new Numbers();
        try {
            WorkWithNumbers.serialize(n);

            Class aClass = Class.forName("lab7.task1.LoadClass");
            Object loadc = aClass.newInstance();
            Method method = aClass.getMethod("work", String.class);
            method.invoke(loadc, "numbers.txt");

            n = WorkWithNumbers.deserialize("numbers.txt");
        } catch (IOException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        } catch (IllegalAccessException e) {
            System.out.println(e);
        } catch (InstantiationException e) {
            System.out.println(e);
        } catch (InvocationTargetException e) {
            System.out.println(e);
        } catch (NoSuchMethodException e) {
            System.out.println(e);
        }

        Assert.assertEquals(5, n.num1);
        Assert.assertEquals(55, n.num2);
        Assert.assertEquals(555, n.num3);
    }
}