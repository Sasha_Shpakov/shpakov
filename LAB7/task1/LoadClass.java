package lab7.task1;

import java.io.*;

public class LoadClass {
    public void work(String file){
        ObjectInputStream in = null;
        Numbers n = null;
        try {
            in = new ObjectInputStream(new FileInputStream(file));
            n = (Numbers)in.readObject();
        } catch (IOException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }

        n.num1 = 5;
        n.num2 = 55;
        n.num3 = 555;

        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream(file));
            out.writeObject(n);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            try {
                if (out != null) out.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }


}