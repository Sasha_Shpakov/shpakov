package lab7.task1;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WorkWithNumbers {
    public static void main(String args[]) {
        Numbers n = new Numbers();
        try {
            serialize(n);
        } catch (IOException e) {
            System.out.println(e);
        }


        try {
            Class aClass = Class.forName("lab7.task1.LoadClass");
            Object loadc = aClass.newInstance();
            Method method = aClass.getMethod("work", String.class);
            method.invoke(loadc, "numbers.txt");
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        } catch (IllegalAccessException e) {
            System.out.println(e);
        } catch (InstantiationException e) {
            System.out.println(e);
        } catch (InvocationTargetException e) {
            System.out.println(e);
        } catch (NoSuchMethodException e) {
            System.out.println(e);
        }

        try {
            deserialize("numbers.txt");
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void serialize(Numbers n) throws IOException {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream("numbers.txt"));
            out.writeObject(n);
        } catch (IOException e) {
            System.out.println(e);
        } finally {
            try {
                if (out != null) out.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }

    public static Numbers deserialize(String file) throws IOException {
        ObjectInputStream in = null;
        Numbers n = null;
        try {
            in = new ObjectInputStream(new FileInputStream("numbers.txt"));
            n = (Numbers) in.readObject();
        } catch (IOException e) {
            System.out.println(e);
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        } finally {
            try {
                if (in != null) in.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
        return n;
    }

}