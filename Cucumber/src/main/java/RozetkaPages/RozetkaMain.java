package RozetkaPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaMain {
    private WebDriver driver;
    public RozetkaMain (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy (xpath = ".//div[@class='logo']")
    public WebElement img_RozetkaMain_Logo;

    @FindBy (xpath = ".//ul[@id='m-main']/li[a[contains(text(),'Apple')]]")
    public WebElement lnk_RozetkaMain_Apple;

    @FindBy (xpath = ".//ul[@id='m-main']/li[a[contains(text(),'MP3')]]")
    public WebElement lnk_RozetkaMain_MP3;

    @FindBy (xpath = ".//span[@class='xhr bold']")
    public WebElement lnk_RozetkaMain_City;

    @FindBy (xpath = ".//div[contains(@id,'cart_block')]")
    public WebElement lnk_RozetkaMain_Bin;

    public RozetkaCity navigateToCity() {
        lnk_RozetkaMain_City.click();
        return new RozetkaCity(driver);
    }

    public RozetkaBin navigateToBin() {
        lnk_RozetkaMain_Bin.click();
        return new RozetkaBin(driver);
    }
}
