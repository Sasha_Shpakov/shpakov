package StackoverflowPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowSignUp {
    private WebDriver driver;
    public StackoverflowSignUp (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div[@id='openid-buttons']//span[text()='Google']")
    public WebElement btn_SignUp_Google;

    @FindBy(xpath = ".//div[@id='openid-buttons']//span[text()='Facebook']")
    public WebElement btn_SignUp_Facebook;
}
