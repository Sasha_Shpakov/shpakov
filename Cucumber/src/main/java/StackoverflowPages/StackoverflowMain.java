package StackoverflowPages;

import RozetkaPages.RozetkaCity;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.*;

public class StackoverflowMain {
    private WebDriver driver;
    public StackoverflowMain (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//span[@class='bounty-indicator-tab']")
    public WebElement lnk_StackMain_Number;

    @FindBy(xpath = ".//span[@class='topbar-menu-links']/a[text()='sign up']")
    public WebElement lnk_StackMain_SignUp;

    @FindBy(xpath = ".//div[@id='question-mini-list']/div[1]//h3")
    public WebElement lnk_StackMain_Question;

    @FindBy(xpath = ".//div[@class='title']")
    public WebElement elem_StackMain_Job;

    public int getNumber() {
        return Integer.parseInt(lnk_StackMain_Number.getText());
    }

    public StackoverflowSignUp navigateToSignUp() {
        lnk_StackMain_SignUp.click();
        return new StackoverflowSignUp(driver);
    }

    public StackoverflowQuestion navigateToQuestion() {
        lnk_StackMain_Question.click();
        return new StackoverflowQuestion(driver);
    }

    public String getJobTitle() {
        return elem_StackMain_Job.getText();
    }

    public void checkSalary() {
        int i = getJobTitle().lastIndexOf('$');
        if (i >= 0) {
            int salary = Integer.parseInt(getJobTitle().substring(i + 1, getJobTitle().length() - 1));
            assertTrue("There is no job offer with salary more than $100K", salary > 100);
        }
        else {
            System.out.println("There is no job offer");
        }
    }
}
