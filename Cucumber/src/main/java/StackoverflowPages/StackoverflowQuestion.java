package StackoverflowPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowQuestion {
    private WebDriver driver;
    public StackoverflowQuestion (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='qinfo']//b")
    public WebElement lnk_Question_Date;

    public String getDay() {
        return lnk_Question_Date.getText();
    }
}
