package lab4.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab4.run.TestRunner;
import static org.junit.Assert.*;


public class MyStepdefs {
    @Given("^I am on Rozetka main page$")
    public void iAmOnRozetkaMainPage() throws Throwable {
        if (!TestRunner.driver.getCurrentUrl().equals("http://rozetka.com.ua/")) {
            TestRunner.driver.get("http://rozetka.com.ua/");
        }
    }

    @Then("^I see logo$")
    public void iSeeLogo() throws Throwable {
        assertTrue("Logo isn't displayed", TestRunner.rozetkaMain.img_RozetkaMain_Logo.isDisplayed());
    }

    @Then("^I see Apple menu$")
    public void iSeeAppleMenu() throws Throwable {
        assertTrue("Unable to find Apple", TestRunner.rozetkaMain.lnk_RozetkaMain_Apple.isDisplayed());
    }

    @Then("^I see MPT menu$")
    public void iSeeMPMenu() throws Throwable {
        assertTrue("Unable to find MP3", TestRunner.rozetkaMain.lnk_RozetkaMain_MP3.isDisplayed());
    }

    @When("^I click City link$")
    public void iClickCityLink() throws Throwable {
        TestRunner.rozetkaCity = TestRunner.rozetkaMain.navigateToCity();
    }

    @Then("^I see Kharkiv, Kiev and Odessa cities$")
    public void iSeeKharkivKievAndOdessaCities() throws Throwable {
        assertTrue("Unable to find Kharkiv", TestRunner.rozetkaCity.lnk_CityPopup_CityKharkiv.isDisplayed());
        assertTrue("Unable to find Odessa", TestRunner.rozetkaCity.lnk_CityPopup_CityOdessa.isDisplayed());
        assertTrue("Unable to find Kiev", TestRunner.rozetkaCity.lnk_CityPopup_CityKiev.isDisplayed());
    }

    @When("^I click Bin link$")
    public void iClickBinLink() throws Throwable {
        TestRunner.rozetkaBin = TestRunner.rozetkaMain.navigateToBin();
    }

    @Then("^I see that bin is blank$")
    public void iSeeThatBinIsBlank() throws Throwable {
        assertTrue("Bin isn't empty", TestRunner.rozetkaBin.elem_BinPopup_Empty.isDisplayed());
    }

    @Given("^I am on Stackoverflow main page$")
    public void iAmOnStackoverflowMainPage() throws Throwable {
        if (!TestRunner.driver.getCurrentUrl().equals("http://stackoverflow.com/")) {
            TestRunner.driver.get("http://stackoverflow.com/");
        }
    }

    @Then("^I see that featured number is more than (\\d+)$")
    public void iSeeThatFeaturedNumberIsMoreThan(int number) throws Throwable {
        assertTrue("Number is less than or equal 300", TestRunner.stackoverflowMain.getNumber() > number);
    }

    @When("^I click SignUp link$")
    public void iClickSignUpLink() throws Throwable {
        TestRunner.stackoverflowSignUp = TestRunner.stackoverflowMain.navigateToSignUp();
    }

    @Then("^I see Facebook and Google buttons$")
    public void iSeeFacebookAndGoogleButtons() throws Throwable {
        assertTrue("Unable to find Google button", TestRunner.stackoverflowSignUp.btn_SignUp_Google.isDisplayed());
        assertTrue("Unable to find Facebook button", TestRunner.stackoverflowSignUp.btn_SignUp_Facebook.isDisplayed());
    }

    @When("^I click first Question link$")
    public void iClickFirstQuestionLink() throws Throwable {
        TestRunner.stackoverflowQuestion = TestRunner.stackoverflowMain.navigateToQuestion();
    }

    @Then("^I see that question was asked today$")
    public void iSeeThatQuestionWasAskedToday() throws Throwable {
        assertTrue("Question wasn't asked today", TestRunner.stackoverflowQuestion.getDay().equals("today"));
    }

    @Then("^I see that there is job offer with salary more than (\\d+)k$")
    public void iSeeThatThereIsJobOfferWithSalaryMoreThanK(int arg0) throws Throwable {
        TestRunner.stackoverflowMain.checkSalary();
    }
}


