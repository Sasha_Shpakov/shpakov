@test1 @test3
Feature: Rozetka testing

  Background:
    Given I am on Rozetka main page

  @test2
  Scenario: 001 checking of logo
    Then I see logo

  @test2
  Scenario: 002 checking of Apple menu
    Then I see Apple menu

  Scenario: 003 checking of Apple menu
    Then I see MPT menu

  Scenario: 004 checking of available cities
    When I click City link
    Then I see Kharkiv, Kiev and Odessa cities

  Scenario: 005 checking of bin
    When I click Bin link
    Then I see that bin is blank

