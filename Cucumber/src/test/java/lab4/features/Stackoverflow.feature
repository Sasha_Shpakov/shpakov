@test1 @test4
Feature: Stackoverflow testing

  Background:
    Given I am on Stackoverflow main page

  @test2
  Scenario: 001 checking of featured number
    Then I see that featured number is more than 300

  @test2
  Scenario: 002 checking of social network buttons
    When I click SignUp link
    Then I see Facebook and Google buttons

  Scenario: 003 checking of question's day
    When I click first Question link
    Then I see that question was asked today

  Scenario: 004 checking of jof offers with salary more than $100k
    Then I see that there is job offer with salary more than 100k



