package lab4.run;

import RozetkaPages.*;
import StackoverflowPages.StackoverflowMain;
import StackoverflowPages.StackoverflowQuestion;
import StackoverflowPages.StackoverflowSignUp;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)

@CucumberOptions (
        features = "src/test/java/lab4/features",
        glue = "lab4/steps",
        tags = "@test1, @test2, @test3, @test4"
)

public class TestRunner {
    public static WebDriver driver;
    public static RozetkaMain rozetkaMain;
    public static RozetkaCity rozetkaCity;
    public static RozetkaBin rozetkaBin;
    public static StackoverflowMain stackoverflowMain;
    public static StackoverflowQuestion stackoverflowQuestion;
    public static StackoverflowSignUp stackoverflowSignUp;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        rozetkaMain = new RozetkaMain(driver);
        stackoverflowMain = new StackoverflowMain(driver);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}
