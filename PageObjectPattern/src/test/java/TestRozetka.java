import org.junit.*;
import org.openqa.selenium.WebDriver;
import RozetkaPages.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class TestRozetka {
    private static WebDriver driver;
    private static RozetkaMain rozetkaMain;
    private RozetkaCity rozetkaCity;
    private RozetkaBin rozetkaBin;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        rozetkaMain = new RozetkaMain(driver);
    }

    @Before
    public void setUpBefore() throws Exception {
        driver.get("http://rozetka.com.ua/");
    }

    @After
    public void tearDownAfter() throws Exception {
        System.out.println("Test completed");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.close();
    }

    @Test
    public void CheckLogo() throws Exception {
        assertTrue("Logo isn't displayed", rozetkaMain.img_RozetkaMain_Logo.isDisplayed());
    }

    @Test
    public void CheckApple() throws Exception {
        assertTrue("Unable to find Apple", rozetkaMain.lnk_RozetkaMain_Apple.isDisplayed());
    }

    @Test
    public void CheckMP3() throws Exception {
        assertTrue("Unable to find MP3", rozetkaMain.lnk_RozetkaMain_MP3.isDisplayed());
    }

    @Test
    public void CheckCities() throws Exception {
        rozetkaCity = rozetkaMain.navigateToCity();
        assertTrue("Unable to find Kharkiv", rozetkaCity.lnk_CityPopup_CityKharkiv.isDisplayed());
        assertTrue("Unable to find Odessa", rozetkaCity.lnk_CityPopup_CityOdessa.isDisplayed());
        assertTrue("Unable to find Kiev", rozetkaCity.lnk_CityPopup_CityKiev.isDisplayed());
    }

    @Test
    public void CheckBin() throws Exception {
        rozetkaBin = rozetkaMain.navigateToBin();
        assertTrue("Bin isn't empty", rozetkaBin.elem_BinPopup_Empty.isDisplayed());
    }
}