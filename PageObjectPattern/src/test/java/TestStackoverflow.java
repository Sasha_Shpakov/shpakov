import StackoverflowPages.*;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class TestStackoverflow {
    private static WebDriver driver;
    private static StackoverflowMain stackoverflowMain;
    private StackoverflowQuestion stackoverflowQuestion;
    private StackoverflowSignUp stackoverflowSignUp;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        stackoverflowMain = new StackoverflowMain(driver);
    }

    @Before
    public void setUpBefore() throws Exception {
        driver.get("http://stackoverflow.com/");
    }

    @After
    public void tearDownAfter() throws Exception {
        System.out.println("Test completed");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.close();
    }

    @Test
    public void CheckNumber() throws Exception {
        assertTrue("Number is less than or equal 300", stackoverflowMain.getNumber()>300);
    }

    @Test
    public void CheckSignUp() throws Exception {
        stackoverflowSignUp = stackoverflowMain.navigateToSignUp();
        assertTrue("Unable to find Google button", stackoverflowSignUp.btn_SignUp_Google.isDisplayed());
        assertTrue("Unable to find Facebook button", stackoverflowSignUp.btn_SignUp_Facebook.isDisplayed());
    }

    @Test
    public void CheckQuestion() throws Exception {
        stackoverflowQuestion = stackoverflowMain.navigateToQuestion();
        assertTrue("Question wasn't asked today", stackoverflowQuestion.getDay().equals("today"));
    }

    @Test
    public void CheckOffers() throws Exception {
        stackoverflowMain.checkSalary();
    }
}