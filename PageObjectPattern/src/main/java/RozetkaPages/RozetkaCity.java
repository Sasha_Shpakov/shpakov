package RozetkaPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaCity {
    private WebDriver driver;
    public RozetkaCity (WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div[@class='clearfix']//*[contains(text(),'Харьков')]")
    public WebElement lnk_CityPopup_CityKharkiv;

    @FindBy (xpath = ".//div[@class='clearfix']//*[contains(text(),'Одесса')]")
    public WebElement lnk_CityPopup_CityOdessa;

    @FindBy (xpath = ".//div[@class='clearfix']//*[contains(text(),'Киев')]")
    public WebElement lnk_CityPopup_CityKiev;
}
