package lab3.task5;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab3Task5 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        System.out.println("Введите целые числа, после каждого нажимайте Enter. После ввода всех чисел введите 'сумма' и программа посчитает сумму всех введенных чисел.");
        while (true) {
            String enter = reader.readLine();
            if (enter.equals("сумма")) {
                break;
            }
            int enter_i = Integer.parseInt(enter);
            sum = sum + enter_i;

        }
        System.out.println("Сумма всех введенных чисел = " + sum);
    }
}
