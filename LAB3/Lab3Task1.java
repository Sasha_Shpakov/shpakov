package lab3.task1;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab3Task1 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] x = new int[4];
        System.out.println("Введите четыре целых числа, после каждого нажимайте Enter");
        for (int i = 0; i < x.length; i++) {
            x[i] = Integer.parseInt(reader.readLine());
        }
        int min = x[0];
        for (int i = 1; i < x.length; i++) {
            if (x[i] < min) {
                min = x[i];
            }
        }
        System.out.println("Наименьшее число из введенных = " + min);
    }
}
