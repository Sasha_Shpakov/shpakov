package lab3.task3;


import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab3Task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите целое число. Программа выведет все простые числа до него включительно");
        int N = Integer.parseInt(reader.readLine());
        System.out.println("Простые числа в диапазоне от 1 до " + N + ":");
        System.out.println(2);
        for (int i = 3; i <= N; i+=2) {
            boolean prime = true;
            for (int j = 3; j < i/2; j+=2) {
                if (i%j == 0) {
                    prime = false;
                    break;
                }
            }
            if (prime) {
                System.out.println(i);
            }
        }
    }
}
