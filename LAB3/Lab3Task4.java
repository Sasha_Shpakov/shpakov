package lab3.task4;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lab3Task4 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] x = new int[4];
        System.out.println("Введите четыре целых числа, после каждого нажимайте Enter");
        for (int i = 0; i < x.length; i++) {
            x[i] = Integer.parseInt(reader.readLine());
        }
        System.out.print("Введенные числа: ");
        for (int i : x)
        {
            System.out.print(i + " ");
        }
    }
}
